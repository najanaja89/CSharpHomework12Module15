﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace CSharpHomework12Module15
{
    public class Item
    {
        public string Name { get; set; }
        public string Color { get; set; }
        public string Model { get; set; }
    }
}