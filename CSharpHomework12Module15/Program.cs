﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace CSharpHomework12Module15
{
    class Program
    {
        static void Main(string[] args)
        {
            string menu = "";
            Console.WriteLine("Press 1 to task 1");
            Console.WriteLine("Press 2 to task 2");
            menu = Console.ReadLine();
            switch (menu)
            {
                case "1":
                    Type type = typeof(Console);
                    foreach (var method in type.GetMethods())
                    {
                        Console.WriteLine(method);
                    }
                    Console.ReadLine();
                    break;

                case "2":
                    Item item = new Item()
                    {
                        Name = "Item1",
                        Color = "Red",
                        Model = "Model1"
                    };

                    //Console.WriteLine(item.Color);

                    Type typeItem = item.GetType();

                    foreach (var field in typeItem.GetProperties())
                    {
                        Console.WriteLine($"{field.Name}\t{field.GetValue(item)}");
                    }
                    Console.ReadLine();
                    break;

                default:
                    break;
            }

        }
    }
}
